SEZIONE V – Finanze
====================


Articolo 18 – Patrimonio di Volt Italia
----------------------------------------

1. Il patrimonio di Volt è costituito da:
	
	a) quote associative;
	
	b) contributi ed erogazioni liberali;
	
	c) investimenti mobiliari e immobiliari;
	
	d) interessi attivi e altre rendite patrimoniali;
	
	e) l’utile derivante da manifestazioni o partecipazioni ad esse;
	
	f) eredità, legati, donazioni, lasciti o successioni;
	
	g) beni, immobili, beni registrati mobili e beni mobili di proprietà, ovunque si trovino, acquistati direttamente da Volt, dalle sue organizzazioni territoriali locali, o comunque pervenuti;
	
	h) ogni altro tipo di entrata consentita dalla legge.


2. Volt utilizza il patrimonio per il perseguimento delle proprie finalità e dei propri obiettivi e per sostenere le spese necessarie al suo funzionamento. Il patrimonio può essere altresì utilizzato per sostenere le eventuali articolazioni territoriali di Volt Italia.

3. Le erogazioni liberali in denaro e le donazioni sono accettate dal Consiglio Direttivo o da organo appositamente preposto a ciò, nel rispetto della politica sulle donazioni di cui al presente Statuto.

4. I lasciti testamentari sono accettati, con beneficio d'inventario, dal Consiglio Direttivo o da organo appositamente preposto a ciò.

5. Durante la vita di Volt non possono essere distribuiti agli iscritti, neanche in modo indiretto, eventuali utili o avanzi di gestione, nonché fondi, riserve o capitale.

6. L’anno sociale e l’anno finanziario vanno dal 1° gennaio al 31 dicembre di ogni anno.


Articolo 19 – Quota associativa
--------------------------------

1. Ciascun membro è tenuto al pagamento annuale della quota associativa. 

2. L’ammontare della quota è determinata annualmente dal Consiglio Direttivo su proposta del Tesoriere. 3. La quota associativa potrà essere eventualmente differenziata a seconda delle situazioni lavorative o del reddito di classi di membri, in modo da facilitare la vita associativa per i soggetti con minori disponibilità economiche. Rientra nelle facoltà del Consiglio Direttivo la scelta dei criteri per la valutazione della sussistenza dei requisiti per il pagamento di una quota associativa differenziata.

Articolo 20 – Politica sulle donazioni
---------------------------------------


1. Il Tesoriere, in conformità con la normativa applicabile, pubblica annualmente sul sito internet di Volt Italia la lista delle donazioni ricevute il cui importo superi Euro 3.000,00 o che, sommate ad altre donazioni, porti il totale delle donazioni ricevute da una singola fonte, nell’arco dei precedenti 12 mesi, a superare l’importo di Euro 3.000,00. Nella pubblicazione sono specificati sia i singoli donanti, sia l’importo donato. 

2. Qualora una singola donazione superi l’importo di Euro 10.000,00 oppure superi l’importo di Euro 3.000,00 ed avvenga entro 6 mesi dalle elezioni del Parlamento Europeo o del Parlamento nazionale, la relativa pubblicazione sul sito internet di Volt Italia avviene entro e non oltre 30 giorni dal ricevimento della specifica donazione, senza attendere la pubblicazione annuale di cui al precedente paragrafo.