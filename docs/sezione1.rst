SEZIONE I – Disposizioni Generali
=================================

Articolo 1 – Costituzione, sede e fonti di disciplina
------------------------------------------------------

1. È costituita, ai sensi del Libro I, Titolo II, Capo III, del Codice Civile, un’associazione politica volontaristica non riconosciuta senza fini di lucro denominata «Volt Italia» (di seguito indicata anche come «Volt» o «Associazione»).

2. Volt ha sede legale in Milano. Il trasferimento della sede legale nell’ambito dello stesso Comune non comporta modifica statutaria e può essere approvato dal Consiglio Direttivo di Volt Italia. La sede legale dell’Associazione non può essere trasferita al di fuori del territorio della Repubblica italiana.

3. Volt è disciplinata dal presente Statuto, dai regolamenti e dalle deliberazioni e dalle decisioni adottate dagli organi di Volt e agisce nel rispetto delle leggi vigenti.

4. Il logo di Volt Italia è allegato al presente Statuto come Allegato A. Volt Italia è contraddistinta da un logo a sfondo viola al cui interno è presente la scritta “Volt” di colore bianco e da variazioni dello stesso, con o senza l’aggiunta di indicazioni geografiche e/o di rimandi alla bandiera italiana e a quella dell’Unione Europea. Spetta al Consiglio Direttivo dell’Associazione la tutela del logo di Volt Italia.
