SEZIONE VI – Miscellanea
=========================

Articolo 21 – Politica sulla privacy
-------------------------------------

Volt Italia conduce le proprie attività nel pieno rispetto di tutta la normativa italiana ed europea in materia di protezione dei dati personali. In particolare Volt si adegua alle disposizioni di cui al Codice della Privacy e successive modifiche e alle disposizioni di cui al Regolamento (UE) 2016/679 e successivi interventi normativi di recepimento e implementazione dello stesso. Volt assicura che solo i dati personali dei propri membri rilevanti e necessari allo svolgimento delle attività di Volt e al rispetto di obblighi di legge vengano trattati, ciò sempre e comunque nel rispetto della suddetta normativa e con l’adozione di tutte le misure necessarie per assicurare la sicurezza di tali dati.


Articolo 22 – Uguaglianza di genere
------------------------------------

1. I membri si impegnano a adempiere pienamente alle norme relative alla tutela del genere meno rappresentato, di volta in volta in vigore.
Al fine di promuovere la diversità e l’equilibrio tra i generi all'interno di Volt, valori in cui l’associazione crede fermamente, in tutti gli organi collegiali (esclusa ovviamente l’Assemblea Generale) il genere meno rappresentato dovrà essere comunque rappresentato da almeno il 33% dei membri, calcolato arrotondando per eccesso all'intero più vicino.

2. Qualora la suddetta quota non venisse raggiunta naturalmente, l’ultimo membro eletto o selezionato in ordine cronologico del genere più rappresentato dovrà essere sostituito d’ufficio senza indugio con il primo candidato del sesso meno rappresentato in lista per il medesimo ruolo. Il presente procedimento di sostituzione dovrà ripetersi fino al soddisfacimento del requisito di cui al presente articolo.

3. Questo articolo non pregiudica eventuali norme più restrittive che dovessero di volta in volta essere in vigore in relazione a situazioni o organi specifici.


Articolo 23 – Durata e scioglimento
------------------------------------

1. La durata di Volt è a tempo indeterminato.

2. Potrà tuttavia essere sciolta in qualsiasi momento per cessazione dell'attività o per qualunque altra causa. Lo scioglimento è deliberato dall'Assemblea Generale con il voto favorevole di almeno i 3/4 dei membri aventi diritto di voto.

3. In caso di scioglimento, per qualunque causa, di Volt le eventuali attività residue potranno essere devolute dall'Assemblea Generale solo ad altre organizzazioni con finalità analoghe o a fini di pubblica utilità, salvo diversa destinazione imposta dalla di legge.


Articolo 24 – Controversie, scelta del foro e legge applicabile
----------------------------------------------------------------

1. Il presente atto è interpretato e regolato secondo la legge italiana.

2. Fatta salva la facoltà di ciascun membro di adire il Collegio dei Probiviri, tutte le questioni relative a sanzioni, associazione, interpretazione e applicazione dello Statuto, regolamenti interni di Volt Italia, nonché le questioni relative a controversie tra i membri e Volt Italia stessa saranno risolte mediante arbitrato rituale, di diritto, amministrato secondo il Regolamento della Camera Arbitrale di Milano, da un arbitro unico, nominato in conformità a tale Regolamento. Il diritto applicabile a qualsiasi controversia, indipendentemente dalla nazionalità delle parti coinvolte, sarà il diritto della Repubblica italiana.

3. Per ogni controversia non arbitrabile, e unicamente per tali controversie, senza che ciò possa valere o essere interpretato in modo incompatibile con il precedente paragrafo, è esclusivamente competente il Foro di Milano.