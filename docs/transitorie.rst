DISPOSIZIONI TRANSITORIE
===========================

1. Entro sei mesi dall'approvazione del presente Statuto è convocata l’Assemblea per il rinnovo del Consiglio Direttivo. Sino a tale elezione, resta in carica il Consiglio Direttivo eletto il 14 luglio 2018.

2. Fintanto che non verrà costituito il Consiglio Strategico, l’Assemblea approva tutti i regolamenti (ivi incluso quello relativo al riconoscimento delle articolazioni territoriali) nonché le policy di Volt Italia, come definite dal Consiglio Direttivo. Fino alla nomina del Presidente del Consiglio Strategico, l’Assemblea è presieduta da uno dei co-Presidenti di Volt Italia.