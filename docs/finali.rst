DISPOSIZIONI FINALI
====================

1. Il Consiglio Direttivo corregge eventuali errori materiali o difetti di coordinamento tra gli articoli, contenuti nel presente Statuto.

2. L’Assemblea si riunisce la prima volta in veste costituente in data 14 luglio 2018. 3. Il presente Statuto di Volt viene letto, approvato e sottoscritto dai membri che ne hanno sottoscritto l’Atto Costitutivo, nonché indirettamente da tutti i membri iniziali come elencati nell'Allegato D al presente Statuto, che non corrisponde al Registro dei membri