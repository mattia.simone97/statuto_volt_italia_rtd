SEZIONE III – Status di membro
=================================

Articolo 3 – Adesione a Volt Italia
--------------------------------------

1. Il numero dei membri di Volt Italia è illimitato, ma non può essere inferiore a tre (3).

2. L’adesione a Volt è libera. Possono aderire a Volt, conseguendo la qualifica di membro, tutte le persone, senza alcuna distinzione di sesso, razza, idee, religione o condizione sociale, che ne condividono le finalità e si impegnano a rispettare il Manifesto, lo Statuto e ogni atto e regolamento adottato ai sensi di quest’ultimo.

Articolo 4 – Requisiti per divenire membro di Volt Italia
----------------------------------------------------------

1. Per associarsi a Volt Italia, l’aspirante membro deve presentare richiesta di adesione compilando l’apposito modulo disponibile presso l’articolazione territoriale di riferimento o sul sito internet di Volt Italia. Al momento della compilazione, l’aspirante membro deve fornire:

	a) i propri dati personali;

	b) una completa informativa sui procedimenti penali pendenti o conclusi nei suoi confronti;

	c) ogni informazione relativa alla propria affiliazione a qualsivoglia altro partito od organizzazione politica;

	d) ogni informazione relativa ad azioni passate, presenti o pianificate per il futuro che possano costituire un rischio grave o una minaccia per la missione e le operazioni di Volt;

	e) ogni altra informazione che venga richiesta ai sensi dei regolamenti adottati da Volt Italia.

2. Le richieste di adesione a Volt in qualità di membro presentate da soggetti di età compresa tra i 16 e i 18 anni dovranno essere controfirmate da un titolare della responsabilità genitoriale.

3. La richiesta di adesione si intende accolta se, entro 2 mesi decorrenti dal giorno della compilazione del modulo, il Consiglio Direttivo, o il soggetto da questo delegato, non comunica all'aspirante membro la propria opposizione. Durante il periodo indicato, il Consiglio Direttivo ha la facoltà di chiedere all'aspirante membro di fornire informazioni aggiuntive.

4. I membri di Volt Italia non possono essere contemporaneamente iscritti ad altro partito politico italiano o europeo, ad eccezione di Volt Europa e delle altre associazioni che fanno capo a Volt, salvo in ipotesi eccezionali debitamente motivate dal Consiglio Direttivo. Al momento della presentazione della richiesta di adesione, l’aspirante membro si impegna a ritirare o revocare la propria adesione ad altri partiti politici entro 2 mesi, ritenendosi altrimenti ritirata, alla scadenza del suddetto periodo, la propria richiesta di adesione a Volt.

5. L’avvenuta adesione è riportata nel Registro dei membri ed è comunicata all’interessato. Il Registro dei membri è tenuto e conservato dal Consiglio Direttivo con modalità telematiche e in modalità analogica presso la sede di Volt. Il Registro dei membri è aggiornato almeno ogni tre mesi e comunque prima di ogni Assemblea. Ogni membro può richiedere di prendere visione del Registro dei membri in ogni momento.

Articolo 5 – Diritti dei membri di Volt Italia
-----------------------------------------------

1. Ai membri di Volt Italia sono riconosciuti i seguenti diritti:

	a) diritto di voto nell’Assemblea di Volt Italia, secondo le modalità e i limiti stabiliti nel presente Statuto e nei regolamenti;

	b) diritto-dovere di partecipare all’attività di Volt manifestando liberamente la propria opinione e la propria critica sugli argomenti in discussione ad ogni livello, eventualmente tramite delegazione secondo le disposizioni del presente Statuto e quelle regolamentari che dovessero venire successivamente adottate;

	c) diritto di utilizzo del simbolo e dei materiali di Volt in buona fede, in conformità con gli scopi di Volt e in osservanza delle regole stabilite dal Consiglio Direttivo o dal Consiglio Strategico;

	d) diritto di presentare la propria candidatura per l’elezione degli organi sociali, nel rispetto delle forme e dei limiti stabiliti dal presente Statuto e dai regolamenti;

	e) diritto di presentare la propria candidatura per la partecipazione alle competizioni elettorali, nel rispetto delle forme e dei limiti stabiliti dal presente Statuto e dai regolamenti.

Articolo 6 – Doveri dei membri di Volt Italia
----------------------------------------------

1. Il comportamento dei membri deve essere conforme alle regole della correttezza e della buona fede.

2. Ogni membro è tenuto ad osservare lo Statuto, i regolamenti, le deliberazioni e le decisioni adottate dagli organi di Volt. È fatta salva la possibilità di continuare a sostenere la propria posizione differente su scelte deliberate all'interno di Volt, nel rispetto della libertà di espressione di ogni membro e del suo diritto alla partecipazione alla vita di Volt.

3. Ogni membro si impegna a promuovere e supportare la causa di Volt e il perseguimento dei suoi obiettivi.

4. Ogni membro chiamato a ricoprire una funzione all'interno di Volt, o in ogni caso a rappresentare Volt in qualsiasi sede, si impegna a farlo in ossequio alla visione politica di Volt, al suo programma e alle sue linee politiche, chiedendo supporto al Consiglio Direttivo in caso di necessità.

5. Ogni membro si impegna a pagare la quota associativa, secondo le modalità e nel rispetto dei termini di cui al presente Statuto e ai regolamenti adottati da Volt Italia.

Articolo 7 – Perdita della qualifica di membro, procedimento disciplinare e sanzioni
-------------------------------------------------------------------------------------

1. La qualifica di membro si perde per: 
	a) recesso; 
	b) espulsione; 
	c) perdita dei requisiti; 
	d) decesso. 
	
2. I membri possono recedere da Volt mediante comunicazione scritta inviata al Consiglio Direttivo a mezzo di posta elettronica. Il recesso ha effetto immediato ed è certificato dalla rimozione del nome del recedente dal Registro dei membri.

3. I membri sono tenuti ad informare per iscritto Volt Italia in caso di modifica di qualsiasi informazione che costituisca un requisito per divenire membro di Volt.

4. I membri che vengano meno ai principi ispiratori di Volt o che violino il presente Statuto, il Codice di Condotta e ogni altro regolamento, nonché le deliberazioni e decisioni adottate dagli organi di Volt, possono essere sottoposti alla procedura sanzionatoria a norma del successivo articolo 8.

5. Le sanzioni applicabili, in via permanente o temporanea, a seconda della gravità del caso, e anche cumulativamente, sono: 
	a) richiamo scritto; 
	b) sospensione dall'esercizio dei diritti riconosciuti al membro; 
	c) sospensione o rimozione dagli incarichi interni a Volt; 
	d) espulsione. Ogni provvedimento sanzionatorio deve essere motivato.

6. Il diritto di voto dei membri non in regola con il versamento della quota associativa è temporaneamente e automaticamente sospeso, salvo ulteriori sanzioni irrogate ai sensi del presente Statuto.

7. I membri receduti, esclusi, deceduti o comunque cessati dalla qualifica di membro non hanno alcun diritto sul patrimonio di Volt e pertanto essi o i loro aventi causa non possono richiedere il rimborso delle quote associative versate.

Articolo 8 – Procedura Sanzionatoria
-------------------------------------

1. Competente a emettere ogni sanzione nei confronti dei membri è il Consiglio Direttivo, o il soggetto a ciò specificamente preposto da quest’ultimo, secondo la procedura prevista dal relativo regolamento, nel rispetto del principio del contraddittorio.

2. Il membro sanzionato ha il diritto di impugnare la decisione dinanzi al Collegio dei Probiviri, il quale delibera ai sensi delle proprie specifiche regole procedurali.

3.Competente ad emettere ogni sanzione nei confronti dei membri che siano componenti del Consiglio Direttivo è il Collegio dei Probiviri.

4. Un membro sanzionato rimane vincolato ad ogni obbligazione finanziaria e di qualsiasi altra natura assunta nei confronti di Volt Italia.
