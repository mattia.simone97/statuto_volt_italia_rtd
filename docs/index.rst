.. Read the Docs Template documentation master file, created by
   sphinx-quickstart on Tue Aug 26 14:19:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Statuto di Volt Italia
==================================================

.. toctree::
   
   sezione1.rst
   sezione2.rst
   sezione3.rst
   sezione4.rst
   sezione5.rst
   sezione6.rst
   transitorie.rst
   finali.rst
