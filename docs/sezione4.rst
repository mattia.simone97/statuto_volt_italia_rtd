SEZIONE IV – Organi di Volt Italia
==================================

Articolo 9 – Elenco degli organi di Volt Italia
------------------------------------------------

Sono organi di Volt Italia:

	+ l’Assemblea Generale
	+ il Consiglio Strategico
	+ il Consiglio Direttivo
	+ i Co-Presidenti
	+ il Tesoriere
	+ il Comitato Elettorale
	+ il Collegio dei Probiviri (altrimenti detto Comitato di Risoluzione delle Liti)

Articolo 10 - L’Assemblea Generale
-----------------------------------

1. L'Assemblea Generale dei membri (o semplicemente «Assemblea») è l’organo di indirizzo politico di Volt Italia.

2. L’Assemblea è costituita da tutti i membri con diritto di voto. Hanno diritto a votare solo coloro che alla data di convocazione dell’Assemblea risultino iscritti nel Registro dei membri ed il cui diritto di voto non risulti sospeso per effetto di un provvedimento sanzionatorio o del mancato pagamento della quota associativa.

3. L’Assemblea:

	a) elegge e revoca i componenti del Consiglio Direttivo e del Collegio dei Probiviri;

	b) elegge i rappresentanti di Volt Italia, individuati dal Comitato Elettorale previo parere del Consiglio Direttivo, da candidare alle competizioni elettorali nazionali ed europee o da nominare in seno ad enti od organismi nazionali ed internazionali;

	c) approva il Codice di Condotta e il regolamento dell’Assemblea;

	d) stabilisce i principi e l’indirizzo politico di Volt Italia, definendone le direttive generali;

	e) approva il rendiconto consuntivo e il bilancio preventivo;

	f) determina l’eventuale compenso spettante ai componenti del Consiglio Direttivo;

	g) delibera sulle modificazioni dello Statuto;

	h) delibera sullo scioglimento di Volt Italia e sulla destinazione del patrimonio residuante dalla liquidazione;

	i) decide su ogni altra questione ad essa rimessa dal presente Statuto o dai regolamenti di Volt Italia.


4. L’Assemblea è convocata dal Consiglio Direttivo ogni qual volta esso lo ritenga necessario, nonché quando ne venga fatta richiesta da almeno 1/5 dei membri di Volt Italia aventi diritto di voto. In caso di inerzia del Consiglio Direttivo, l’Assemblea è convocata dal Presidente del Collegio dei Probiviri.

5. L’Assemblea è convocata, almeno una volta all'anno, con preavviso di almeno 16 giorni, o di 8 in caso di urgenza, mediante avviso di convocazione inviato a mezzo email o altri mezzi idonei a tutti i membri con diritto di voto e contenente il luogo, la data, l’ora dell’Assemblea e i punti all'ordine del giorno.

6. L’Assemblea può essere svolta e deliberare online, attraverso strumenti telematici, e/o nella sede di Volt o in qualunque altra sede indicata nell'avviso di convocazione.

7. La direzione dei lavori assembleari spetta al Presidente del Consiglio Strategico, che nomina un segretario per la redazione del verbale, da entrambi sottoscritto.

8. A meno che non sia diversamente stabilito, l’Assemblea decide a maggioranza semplice dei presenti. Per la modifica del Codice di Condotta e per l’approvazione e la modifica dei regolamenti, l’Assemblea è validamente costituita con la presenza, anche da remoto, del 25% più uno degli aventi diritto e decide con il voto favorevole del 55% dei presenti. Per modificare lo Statuto occorre la presenza, anche da remoto, del 50% più uno degli aventi diritto e il voto favorevole del 66% dei presenti.

9. Le modalità di funzionamento dell’Assemblea sono disciplinate nel relativo regolamento nel rispetto del presente Statuto.


Articolo 11- Il Consiglio Strategico
-------------------------------------

1. Il Consiglio Strategico partecipa alla definizione delle strategie politiche di Volt Italia e approva le policy e i regolamenti di Volt, secondo quanto previsto dal presente articolo.
2. Il Consiglio Strategico è composto da:

	a) i componenti il Consiglio Direttivo;
	b) un massimo di 50 rappresentanti territoriali, con un minimo di 5 rappresentanti per ciascuna delle cinque circoscrizioni elettorali previste dalla legge 24 gennaio 1979, n. 18 e successive modificazioni;
	c) un massimo di 50 rappresentanti dei team operativi di Volt Italia;
	d) gli eletti di Volt Italia alle cariche elettive di deputato al parlamento europeo, deputato o senatore del parlamento italiano per la durata del rispettivo mandato;
	e) gli ultimi 2 ex Presidenti di Volt Italia, salvo che siano stati revocati dalla carica o la cui cessazione dalla carica sia dipesa dall’applicazione di una sanzione; I consiglieri di cui alle lettere b. e c. sono eletti secondo le modalità previste in apposito regolamento approvato dall’Assemblea.

3. Il Consiglio Strategico:

	a) approva il regolamento finanziario, il regolamento del Consiglio Strategico, il regolamento per la costituzione e il funzionamento delle articolazioni territoriali di Volt Italia, nonché ogni altro regolamento interno di Volt Italia, che non sia rimesso dal presente Statuto alla competenza di altro organo;

	b) approva le policy di Volt Italia, secondo le direttive generali definite dall’Assemblea su iniziativa del Consiglio Direttivo;

	c) esprime il proprio parere preventivo, obbligatorio e non vincolante, in merito alla partecipazione di Volt alle competizioni elettorali a livello nazionale ed europeo;

	d) decide sulle opposizioni presentate dalle articolazioni territoriali interessate avverso le decisioni del Consiglio Direttivo assunte in materia di coordinamento tra Volt Italia e le articolazioni territoriali;

	e) decide su ogni altra questione ad esso rimessa dai regolamenti interni di Volt Italia.

4. Il Consiglio Strategico è convocato almeno una volta ogni tre mesi dal Consiglio Direttivo, nonché quando ne venga fatta richiesta da almeno 1/5 dei componenti o 1/5 dei membri di Volt Italia aventi diritto di voto. In caso di inerzia del Consiglio Direttivo, il Consiglio Strategico è convocato dal proprio Presidente nominato alla prima riunione dell’organo.

5. Il Consiglio Strategico è convocato senza particolari formalità con preavviso di almeno 3 giorni, salvo in casi di particolare urgenza.

6. Le riunioni del Consiglio Strategico possono essere svolte e le relative deliberazioni possono avere luogo online, attraverso strumenti telematici, e/o nella sede legale di Volt o in qualunque altra sede fisica indicata.

7. La direzione dei lavori del Consiglio Strategico spetta al Presidente dell’organo, che nomina un segretario per la redazione del verbale, da entrambi sottoscritto.

8. A meno che non sia diversamente stabilito, il Consiglio Strategico è validamente costituito con la presenza della metà più uno dei componenti e decide a maggioranza semplice dei presenti. Per l’approvazione dei regolamenti il Consiglio Strategico è validamente costituito con la presenza della metà più uno degli aventi diritto e decide con il voto favorevole dei 3/5 dei presenti.

9. Le modalità di costituzione e funzionamento del Consiglio Strategico sono disciplinate, nel rispetto del presente Statuto, nel relativo regolamento, approvato alla prima occasione utile.


Articolo 12 - Il Consiglio Direttivo
-------------------------------------

1. Il Consiglio Direttivo è l'organo incaricato dell'organizzazione, della gestione e della promozione di Volt Italia, nonché dell'attuazione del suo indirizzo politico.

2. Il Consiglio Direttivo è composto da 9 membri di cui:

	a) 2 co-Presidenti di genere diverso;

	b) 1 Tesoriere;
	
	c) 5 consiglieri, ciascuno espressione di una delle circoscrizioni elettorali previste dalla legge 24 gennaio 1979, n. 18 e successive modificazioni;
	
	d) 1 consigliere con particolare vocazione internazionale/europea in termini di attivismo o di studio o di lavoro.

3. I consiglieri di cui alle lettere a., b. e d. sono eletti ovvero scelti dall'Assemblea secondo un criterio che tenga conto del voto o del giudizio espresso da tutti i membri di Volt Italia. I consiglieri di cui alla lettera c. sono eletti ovvero scelti secondo un criterio che tenga conto del voto o del giudizio espresso dai membri di Volt Italia attivi nella rispettiva circoscrizione. Il consigliere di cui alla lettera b. è eletto tra soggetti con comprovate competenze in materia contabile e economico-finanziaria.

4. Possono essere eletti consiglieri i membri di Volt Italia con diritto di voto, iscritti da almeno 6 mesi.

5. Il mandato dei consiglieri dura 2 (due) anni. I consiglieri sono rieleggibili per un massimo di ulteriori 2 (due) mandati, all'esito dei quali non possono essere rieletti se non previo decorso di due mandati successivi.

6. Il Consiglio Direttivo:

	a) attua l’indirizzo politico di Volt Italia, secondo le direttive generali approvate dall'Assemblea, come sviluppate e approvate dal Consiglio Strategico;

	b) si occupa della gestione quotidiana di Volt Italia, nel rispetto del bilancio approvato dall'Assemblea;
	
	c) garantisce il coordinamento tra Volt Italia, le associazioni nazionali corrispondenti di altri Stati membri dell’Unione Europea, e Volt Europa;
	
	d) assicura il coordinamento tra le articolazioni territoriali e Volt Italia;
	
	e) esprime il proprio parere sulla selezione dei candidati effettuata dal Comitato Elettorale;
	
	f) individua eventuali soggetti esterni a Volt che, in virtù degli specifici requisiti posseduti, possono essere candidati nelle liste di Volt nelle competizioni elettorali;
	
	g) promuove Volt Italia nei diversi ambiti politici, sociali, culturali, artistici ed economici;
	
	h) tiene il Registro dei membri;
	
	i) nomina con delibera i responsabili dei team operativi delegati a ricoprire specifiche funzioni di assistenza al Consiglio Direttivo e al Consiglio Strategico;
	
	j) può delegare ai singoli consiglieri specifiche funzioni rientranti nelle sue competenze. La delega può essere revocata in ogni momento;
	
	k) esegue le decisioni degli organi di Volt;
	
	l) sottopone all'Assemblea il rendiconto consuntivo e il bilancio preventivo per la loro approvazione;
	
	m) può proporre all'Assemblea di deliberare un compenso in favore dei componenti del Consiglio Direttivo, indicando, eventualmente, anche l’entità del compenso proposto;
	
	n) può consultare i membri di Volt Italia su qualsiasi questione o tematica, senza particolari formalità e anche attraverso strumenti telematici; il risultato della consultazione non è vincolante;

	o) delibera su tutte le questioni che non sono rimesse ad altri organi di Volt Italia dalla legge o dal presente Statuto.

7. Il Consiglio Direttivo è convocato senza formalità dai co-Presidenti e adotta le proprie deliberazioni a maggioranza semplice dei propri componenti. Il Consiglio Direttivo può essere altresì convocato su richiesta di un 1/5 dei suoi componenti o del Consiglio Strategico, per discutere di questioni ritenute urgenti. Di ogni riunione del Consiglio Direttivo è redatto e sottoscritto un verbale da parte di uno dei suoi componenti.

8. Le modalità di costituzione e funzionamento del Consiglio Direttivo sono disciplinate nel rispetto del presente Statuto in apposito regolamento approvato dal Consiglio Strategico alla prima occasione utile.

9. La cessazione di uno o più componenti del Consiglio Direttivo dalla carica, per qualunque ragione, comporta l’obbligo per i rimanenti componenti del Consiglio Direttivo (che agiranno ad interim in regime di prorogatio) di convocare urgentemente l’Assemblea Generale per procedere a una nuova elezione del componente/i cessato/i dalla carica.

10. I componenti del Consiglio Direttivo non possono, per tutta la durata del loro mandato, candidarsi a ricoprire cariche elettive all’interno di Volt Italia o di Volt Europa, ad esclusione di quanto previsto dal presente Statuto e dallo Statuto di Volt Europa.

11. La carica di componente del Consiglio Direttivo è incompatibile con la carica di membro del Parlamento Europeo, membro del Parlamento Italiano, componente del governo italiano, presidente di giunta regionale, assessore regionale, consigliere regionale, sindaco di comune con popolazione superiore a 15.000 abitanti. Le modalità di candidatura di un componente del Consiglio Direttivo alla cariche di cui al presente comma sono disciplinate dal regolamento del Consiglio Direttivo.


Articolo 13 - I co-Presidenti
------------------------------

1. I Co-Presidenti sono i Co-leader politici di Volt Italia e, oltre ai poteri loro attribuiti in qualità di componenti del Consiglio Direttivo, hanno i seguenti poteri e doveri:

	a) assicurano il mantenimento, contribuiscono e promuovono la visione politica, il programma e le policy di Volt;
	
	b) gestiscono l’utilizzo del simbolo e degli altri segni distintivi di Volt, anche ai fini dello svolgimento di tutte le attività necessarie alla presentazione delle liste nelle tornate elettorali;
	
	c) svolgono l’attività di rappresentanza politica interna ed esterna a Volt; d) convocano, aggiornano e presiedono il Consiglio Direttivo.

Articolo 14 - Il Comitato Elettorale
------------------------------------

1. Il Comitato Elettorale è l’organo costituito ai fini della selezione dei candidati per le elezioni politiche europee, nazionali, regionali e locali, nonché dei candidati in occasione della costituzione degli organi di Volt Italia e delle sue articolazioni territoriali.

2. Il primo Comitato Elettorale costituito a seguito dell’approvazione del presente Statuto, è composto da 7 membri di Volt Italia scelti dal Collegio dei Probiviri tra coloro che hanno manifestato la propria disponibilità. Il primo Comitato Elettorale resta in carica fino alla costituzione del Consiglio Strategico.

3. Salvo quanto disposto dal precedente comma 2, il Comitato Elettorale è composto da 7 membri di Volt Italia estratti a sorte tra i componenti del Consiglio Strategico che hanno manifestato la propria disponibilità. Il Consiglio Direttivo può nominare, a maggioranza dei 2/3 dei propri componenti, ulteriori 4 componenti del Comitato Elettorale in occasione delle elezioni regionali e locali.

4. Salvo quanto disposto dal precedente comma 2, il Comitato Elettorale resta in carica per un anno a partire dalla sua costituzione e comunque fino al completamento delle eventuali procedure di selezione in corso alla scadenza del mandato.

5. Tutti i componenti del Comitato Elettorale devono essere membri di Volt Italia da almeno 6 mesi.

Articolo 15 – Collegio dei Probiviri
-------------------------------------

1. Il Collegio dei Probiviri (altrimenti detto Comitato di Risoluzione delle Liti o CRL) è l’organo di giustizia interna di Volt Italia; esso è composto da 5 (cinque) componenti effettivi e 2 (due) supplenti, eletti, su indicazione del Consiglio Direttivo, dall'Assemblea Generale per un periodo di 2 (due) anni. I membri del Collegio dei Probiviri sono rieleggibili solo per due mandati consecutivi.

2. Il Collegio dei Probiviri:

	a) approva il regolamento del Collegio dei Probiviri;
	
	b) vigila, in ogni momento, sull'osservanza dello Statuto e dei regolamenti di Volt Italia, inviando segnalazioni costruttive all'organo interessato, e relazionando annualmente l’Assemblea, il Consiglio Strategico e il Consiglio Direttivo sulle violazioni riscontrate;
	
	c) nel rispetto del principio del contraddittorio, giudica sui reclami proposti dagli aspiranti membri avverso l’opposizione alla loro richiesta di adesione di cui all'articolo 4;
	
	d) nel rispetto del principio di contraddittorio, giudica sulle opposizioni presentate dai membri avverso le sanzioni ad essi irrogate in applicazione del presente Statuto, nonché avverso qualunque decisione degli organi di Volt che li riguardi direttamente e individualmente;
	
	e) decide sulle questioni relative alle controversie fra aspiranti membri e Volt e fra i membri di Volt e Volt stessa che non scaturiscono da inammissibilità della richieste di adesione a Volt o da sanzioni;
	
	f) annulla, nell'esercizio delle competenze di cui alle precedenti lettere c) e d), le disposizioni dei regolamenti di Volt Italia contrarie allo Statuto, nonché le deliberazioni adottate da qualunque organo associativo contrarie allo Statuto o ai regolamenti di Volt Italia, qualora tali disposizioni o deliberazioni siano rilevanti ai fini della decisione;
	
	g) annulla, su ricorso del Consiglio Direttivo, del Consiglio Strategico o del 5% dei membri di Volt Italia, le disposizioni dei regolamenti di Volt Italia qualora contrarie allo Statuto, nonché le deliberazioni adottate da qualunque organo associativo qualora contrarie allo Statuto o ai regolamenti di Volt Italia.

3. Le modalità di funzionamento del Collegio dei Probiviri sono disciplinate dal relativo regolamento nel rispetto del presente Statuto.

4. Le decisioni del Collegio dei Probiviri sono vincolanti per tutti i membri e gli organi di Volt Italia.

5. E’ fatta salva la facoltà per i membri di Volt Italia di rivolgere le proprie istanze a organi giurisdizionali esterni a Volt, nel rispetto della legge e della clausola arbitrale di cui al presente Statuto.

Articolo 16 – Tesoriere e Tesoreria
------------------------------------

1. Il Tesoriere è responsabile della regolare gestione amministrativa, patrimoniale, finanziaria e contabile di Volt Italia.

2. Il Tesoriere:

	a) tiene e aggiorna, secondo i principi dell’ordinata contabilità, i registri, i libri e gli altri documenti contabili necessari e cura gli adempimenti formali ad essi connessi, in conformità a quanto previsto dalla legge, dal presente Statuto e dai regolamenti di Volt Italia;
	
	b) gestisce le entrate e le uscite, assicura la regolarità contabile e l’attinenza delle decisioni di spesa degli organi di Volt con le effettive disponibilità bancarie e di cassa e con le voci di bilancio, nel rispetto delle norme di legge in materia;
	
	c) predispone annualmente il rendiconto consuntivo di esercizio e il bilancio preventivo secondo la normativa applicabile e li sottopone al Consiglio Direttivo;
	
	d) nell’ambito delle funzioni allo stesso conferite, può disporre delle somme depositate presso i conti bancari dell’Associazione, sottoscrivere mandati di pagamento, incassare le quote sociali e le erogazioni liberali, tenere i rapporti con le banche e i fornitori in genere e compiere ogni altro adempimento previsto a suo carico dalla legge, dal presente Statuto e dai regolamenti; e) può, in ogni momento, effettuare ispezioni e controlli amministrativi e contabili, relativamente a qualunque articolazione territoriale di Volt. Quando l’esito delle ispezioni e dei controlli rilevi gravi irregolarità, il Consiglio Direttivo può deliberare la sospensione delle erogazioni e irrogare sanzioni disciplinari.

3. Le modalità di formazione del rendiconto consuntivo e del bilancio preventivo, nonché di ogni altro registro, libro o documento contabile sono disciplinate, nel rispetto delle leggi e dei regolamenti applicabili, nel regolamento finanziario e di contabilità approvato dal Consiglio Strategico alla prima occasione utile.

4. Ogni decisione di spesa o di rimborso delle spese sostenute, anche da soggetti terzi, che il Consiglio Direttivo intenda assumere deve essere preventivamente sottoposta al Tesoriere che dovrà verificarne la copertura finanziaria. Qualora la verifica abbia esito favorevole, il Tesoriere rilascerà apposita attestazione e il Consiglio Direttivo potrà deliberare la spesa o il rimborso.

5. Nello svolgimento delle proprie funzioni il Tesoriere può farsi coadiuvare da un apposito ufficio denominato Tesoreria. La Tesoreria è composta da un numero massimo di 5 membri incluso il Tesoriere, che la presiede e ne è responsabile. I membri componenti la Tesoreria sono nominati direttamente dal Tesoriere tra soggetti con comprovate competenze in materia contabile e economico-finanziaria.

Articolo 17 - Rappresentanza legale e potere di firma
------------------------------------------------------

La rappresentanza legale dell’Associazione, sia di fronte a terzi che in giudizio, spetta disgiuntamente ai Co-Presidenti e al Tesoriere. I Co-Presidenti e il Tesoriere hanno altresì il potere di firma, disgiuntamente tra di loro, per tutti gli atti di ordinaria amministrazione inerenti il proprio ufficio e, congiuntamente tra di loro, per tutti gli atti di straordinaria amministrazione.